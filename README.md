
# CRM Core

## Summary

CRM Core Commerce creates and updates crm_core 
individuals after the users has placed his order. 
(commerce_order.place.post_transition) Event is used.

If your individual has additional fields, you can extend the mapper service 
(crm_core_commerce.individual_mapper) and replace the protected 
functions createNewIndividual() and updateIndividual().

## Requirements

Commerce
CRM Core

## Installation

CRM Core Commerce can be installed like any other Drupal module.

1) Download CRM Core to the modules directory for your site.

2) Go to the admin/modules page and enable CRM Core.

Create an email field on the individual entity. (needed as unique identity)

Requires patch for Crm Core
https://www.drupal.org/project/crm_core/issues/3043132

## Contact

Current maintainers:

* Siegrist - https://www.drupal.org/u/siegrist
