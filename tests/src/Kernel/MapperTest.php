<?php

namespace Drupal\Tests\crm_core_commerce\Kernel;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\crm_core_contact\Entity\Individual;
use Drupal\crm_core_contact\Entity\IndividualType;
use Drupal\crm_core_contact\IndividualInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\profile\Entity\Profile;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Tests crm_core_commerce.
 *
 * @group crm_core_commerce
 */
class MapperTest extends CommerceKernelTestBase {

  /**
   * The Mapper Service.
   *
   * @var \Drupal\crm_core_commerce\Mapper\CrmCoreIndividualMapperInterface
   */
  protected $mapperService;

  /**
   * User Sync Relation Manager.
   *
   * @var \Drupal\crm_core_user_sync\CrmCoreUserSyncRelation
   */
  protected $relationManager;

  /**
   * Needed Modules for this test.
   *
   * @var array
   */
  protected static $modules = [
    'user',
    'crm_core_contact',
    'crm_core_user_sync',
    'crm_core_commerce',
    'datetime',
    'name',
    'profile',
    'commerce_order',
    'commerce_number_pattern',
    'entity_reference_revisions',
    'state_machine',
    'address',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    $rules = [];
    parent::setUp();
    $this->installEntitySchema('crm_core_individual');
    $this->installEntitySchema('crm_core_user_sync_relation');
    $this->installEntitySchema('profile');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_number_pattern');

    $this->installConfig([
      'profile',
      'commerce_order',
    ]);

    $this->mapperService = $this->container->get('crm_core_commerce.individual_mapper');
    $this->relationManager = $this->container->get('crm_core_user_sync.relation');

    $this->config('crm_core_commerce.settings')
      ->set('individual_type', 'individual')
      ->save();

    $config = $this->config('crm_core_user_sync.settings');
    $rules[] = [
      'role' => 'customer',
      'contact_type' => 'customer',
      'enabled' => TRUE,
      'weight' => 1,
    ];

    $rules[] = [
      'role' => 'authenticated',
      'contact_type' => 'individual',
      'enabled' => TRUE,
      'weight' => 10,
    ];

    $config
      ->set('rules', $rules)
      ->save();

    $individual_type = IndividualType::create(
      [
        'name' => 'Individual',
        'type' => 'individual',
        'description' => 'A single customer.',
      ]
    );
    $individual_type->setPrimaryFields(['email' => 'field_email']);
    $individual_type->save();
    $field_mail = FieldStorageConfig::create([
      'entity_type' => 'crm_core_individual',
      'field_name' => 'field_email',
      'type' => 'email',
    ]);
    $field_mail->save();

    FieldConfig::create([
      'field_storage' => $field_mail,
      'entity_type' => 'crm_core_individual',
      'bundle' => 'individual',
    ])->save();
  }

  /**
   * Tests the creation of individuals on mapping.
   */
  public function testMapperCreateAndUpdate() {
    $order = Order::create(
      [
        'type' => 'default',
        'mail' => 'test@example.com',
        'order_items' => [],
        'state' => 'completed',
        'ip_address' => '127.0.0.1',
        'order_number' => '6',
        'store_id' => $this->store,
      ]
    );

    $this->assertTrue($order instanceof OrderInterface, 'Its an Order.');

    /** @var \Drupal\profile\Entity\ProfileInterface $billing_profile */
    $billing_profile = Profile::create(
      [
        'type' => 'customer',
        'address' => [
          'given_name' => 'Max',
          'family_name' => 'Muster',
          'country_code' => 'CH',
          'postal_code' => '1234',
        ],
      ]
    );
    $billing_profile->save();

    $this->assertTrue($billing_profile instanceof ProfileInterface, 'Its a Profile.');

    $order->setBillingProfile($billing_profile);
    $this->assertEquals($billing_profile, $order->getBillingProfile(), 'Order has billing profile.');

    $individual = $this->mapperService->mapOrderToIndividual($order);

    $this->assertEquals($individual->name->given, 'Max', 'The Given name was copied correctly.');
    $this->assertEquals($individual->name->family, 'Muster', 'The Family name was copied correctly.');
    $this->assertEquals($individual->get('field_email')->value, 'test@example.com', 'The Email was copied correctly.');

    // Test the update.
    $individual->save();
    $oldId = $individual->id();

    $order = Order::create(
      [
        'type' => 'default',
        'mail' => 'test@example.com',
        'order_items' => [],
        'state' => 'completed',
        'ip_address' => '127.0.0.1',
        'order_number' => '7',
        'store_id' => $this->store,
      ]
    );

    $this->assertTrue($order instanceof OrderInterface, 'Its an Order.');

    /** @var \Drupal\profile\Entity\ProfileInterface $billing_profile */
    $billing_profile = Profile::create(
      [
        'type' => 'customer',
        'address' => [
          'given_name' => 'Max',
          'family_name' => 'Married',
          'country_code' => 'DE',
          'postal_code' => '3233',
        ],
      ]
    );
    $billing_profile->save();
    $order->setBillingProfile($billing_profile);
    $this->assertEquals($billing_profile, $order->getBillingProfile(), 'Order has billing profile.');

    $individual = $this->mapperService->mapOrderToIndividual($order);

    $this->assertEquals($individual->name->given, 'Max', 'The Given name was copied correctly.');
    $this->assertEquals($individual->name->family, 'Married', 'The Family name was copied correctly.');
    $this->assertEquals($individual->get('field_email')->value, 'test@example.com', 'The Email was copied correctly.');

    $individual->save();
    $this->assertEquals($individual->id(), $oldId, 'The Id has not changed on update.');
  }

  /**
   * Tests the mapping engine so the right individuals are mapped.
   */
  public function testMatchingByUser() {
    $account_authenticated = User::create([
      'name' => 'authenticated',
      'uid' => random_int(50, 100),
    ]);
    $account_authenticated->setEmail('tester@zuiop.com');
    $account_authenticated->save();

    $this->assertTrue($account_authenticated instanceof UserInterface);

    $individual_reference = Individual::create([
      'type' => 'individual',
      'name' => [
        'given' => 'Max',
        'family' => 'Muster',
      ],
      'field_email' => 'tester@asdfg.com',
    ]);
    $individual_reference->save();

    $individual_other = Individual::create([
      'type' => 'individual',
      'name' => [
        'given' => 'Someone',
        'family' => 'Else',
      ],
      'field_email' => 'tester@zuiop.com',
    ]);
    $individual_other->save();

    $individual = $this->relationManager->relate($account_authenticated, $individual_reference);

    $this->assertNotNull($individual);
    $this->assertTrue($individual instanceof IndividualInterface);

    $order = Order::create(
      [
        'type' => 'default',
        'mail' => 'tester@zuiop.com',
        'order_items' => [],
        'state' => 'completed',
        'ip_address' => '127.0.0.1',
        'order_number' => '6',
        'store_id' => $this->store,
      ]
    );

    $this->assertTrue($order instanceof OrderInterface, 'Its an Order.');

    $order->set('crm_core_individual', $individual_other);
    $order->setCustomer($account_authenticated);

    $this->assertEquals($order->get('crm_core_individual')->target_id, $individual_other->id());
    $this->assertNotEquals($order->get('crm_core_individual')->target_id, $individual->id());

    /** @var \Drupal\profile\Entity\ProfileInterface $billing_profile */
    $billing_profile = Profile::create(
      [
        'type' => 'customer',
        'address' => [
          'given_name' => 'Max',
          'family_name' => 'Muster',
          'country_code' => 'CH',
          'postal_code' => '1234',
        ],
      ]
    );
    $billing_profile->save();

    $this->assertTrue($billing_profile instanceof ProfileInterface, 'Its a Profile.');

    $order->setBillingProfile($billing_profile);
    $this->assertEquals($billing_profile, $order->getBillingProfile(), 'Order has billing profile.');

    $individualMapped = $this->mapperService->mapOrderToIndividual($order);

    $this->assertEquals($individual->uuid(), $individualMapped->uuid(), 'The Individual was mapped by the relation of the user individual .');
  }

  /**
   * Tests the mapping engine if the individual is set and no user.
   */
  public function testMatchingByIndividual() {
    $individual = Individual::create([
      'type' => 'individual',
      'name' => [
        'given' => 'Max',
        'family' => 'Muster',
      ],
    ]);
    $individual->save();

    Individual::create([
      'type' => 'individual',
      'name' => [
        'given' => 'Someone',
        'family' => 'Else',
      ],
      'field_email' => 'tester@zuiop.com',
    ])->save();

    $order = Order::create(
      [
        'type' => 'default',
        'mail' => 'tester@zuiop.com',
        'order_items' => [],
        'state' => 'completed',
        'ip_address' => '127.0.0.1',
        'order_number' => '6',
        'store_id' => $this->store,
      ]
    );

    $this->assertTrue($order instanceof OrderInterface, 'Its an Order.');

    $order->set('crm_core_individual', $individual);

    $this->assertEquals($order->get('crm_core_individual')->target_id, $individual->id());

    /** @var \Drupal\profile\Entity\ProfileInterface $billing_profile */
    $billing_profile = Profile::create(
      [
        'type' => 'customer',
        'address' => [
          'given_name' => 'Max',
          'family_name' => 'Muster',
          'country_code' => 'CH',
          'postal_code' => '1234',
        ],
      ]
    );
    $billing_profile->save();

    $this->assertTrue($billing_profile instanceof ProfileInterface, 'Its a Profile.');

    $order->setBillingProfile($billing_profile);
    $this->assertEquals($billing_profile, $order->getBillingProfile(), 'Order has billing profile.');

    $individualMapped = $this->mapperService->mapOrderToIndividual($order);

    $this->assertEquals($individual->uuid(), $individualMapped->uuid(), 'The Individual was mapped by the relation of the user individual .');
  }

  /**
   * Tests the mapping engine maps correctly by email.
   */
  public function testMatchingByEmail() {
    $individual = Individual::create([
      'type' => 'individual',
      'name' => [
        'given' => 'Max',
        'family' => 'Muster',
      ],
      'field_email' => 'tester@zuiop.com',
    ]);
    $individual->save();

    $order = Order::create(
      [
        'type' => 'default',
        'mail' => 'tester@zuiop.com',
        'order_items' => [],
        'state' => 'completed',
        'ip_address' => '127.0.0.1',
        'order_number' => '6',
        'store_id' => $this->store,
      ]
    );

    $this->assertTrue($order instanceof OrderInterface, 'Its an Order.');

    /** @var \Drupal\profile\Entity\ProfileInterface $billing_profile */
    $billing_profile = Profile::create(
      [
        'type' => 'customer',
        'address' => [
          'given_name' => 'Max',
          'family_name' => 'Muster',
          'country_code' => 'CH',
          'postal_code' => '1234',
        ],
      ]
    );
    $billing_profile->save();

    $this->assertTrue($billing_profile instanceof ProfileInterface, 'Its a Profile.');

    $order->setBillingProfile($billing_profile);
    $this->assertEquals($billing_profile, $order->getBillingProfile(), 'Order has billing profile.');

    $individualMapped = $this->mapperService->mapOrderToIndividual($order);

    $this->assertEquals($individual->uuid(), $individualMapped->uuid(), 'The Individual was mapped by the relation of the user individual .');
  }

}
