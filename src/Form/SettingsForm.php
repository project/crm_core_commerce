<?php

namespace Drupal\crm_core_commerce\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure general settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity Type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Theme manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager) {
    $this->setConfigFactory($config_factory);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['crm_core_commerce.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'crm_core_commerce_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('crm_core_commerce.settings');

    $form['mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('CRM Commerce Mapping'),
      '#weight' => 1,
    ];
    $individual_types_enities = $this->entityTypeManager
      ->getStorage('crm_core_individual_type')
      ->loadMultiple();
    foreach ($individual_types_enities as $type) {
      $individual_types[$type->id()] = $type->label();
    }

    $form['mapping']['individual_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Order Individual Type'),
      '#description' => $this->t('To this Individual type will be mapped to on commerce orders.'),
      '#default_value' => $config->get('individual_type'),
      '#options' => $individual_types,
      '#weight' => 0,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()
      ->getEditable('crm_core_commerce.settings');

    $config
      ->set('individual_type', $form_state->getValue('individual_type'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
