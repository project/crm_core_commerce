<?php

namespace Drupal\crm_core_commerce\EventSubscriber;

use Drupal\Core\Entity\EntityMalformedException;
use Drupal\crm_core_commerce\Mapper\CrmCoreIndividualMapperInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Order Placed Event Subscriber.
 */
class OrderPlacedSubscriber implements EventSubscriberInterface {

  /**
   * The Mapper Service.
   *
   * @var \Drupal\crm_core_commerce\Mapper\CrmCoreIndividualMapperInterface
   */
  protected $crmIndividualMapper;

  /**
   * Constructs a new EntityRouteProviderSubscriber instance.
   *
   * @param \Drupal\crm_core_commerce\Mapper\CrmCoreIndividualMapperInterface $mapper
   *   The Mapper Service.
   */
  public function __construct(CrmCoreIndividualMapperInterface $mapper) {
    $this->crmIndividualMapper = $mapper;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      'commerce_order.place.pre_transition' => 'saveCrmIndividual',
    ];
  }

  /**
   * Initiate the mapper, validates and saves.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function saveCrmIndividual(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $event->getEntity();

    $individual = $this->crmIndividualMapper->mapOrderToIndividual($order);

    if (!$individual->validate()) {
      throw new EntityMalformedException($individual);
    }

    $individual->save();

    $order->set('crm_core_individual', $individual);
  }

}
