<?php

namespace Drupal\crm_core_commerce\Mapper;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\crm_core_contact\IndividualInterface;

/**
 * Interface CrmCoreIndividualMapper.
 */
interface CrmCoreIndividualMapperInterface {

  /**
   * Creates & updates individuals as well as mapping the order attributes.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The Order.
   *
   * @return \Drupal\crm_core_contact\IndividualInterface
   *   The CRM_Core_individual.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function mapOrderToIndividual(OrderInterface $order): IndividualInterface;

}
