<?php

namespace Drupal\crm_core_commerce\Mapper;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\StorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\crm_core_contact\IndividualInterface;
use Drupal\crm_core_user_sync\CrmCoreUserSyncRelation;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Class CrmCoreIndividualMapper maps commerce_orders into individuals.
 *
 * @package Drupal\crm_core_commerce\Mapper
 */
class CrmCoreIndividualMapper implements CrmCoreIndividualMapperInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * User Sync Relation Manager.
   *
   * @var \Drupal\crm_core_user_sync\CrmCoreUserSyncRelation
   */
  protected $relationManager;

  /**
   * The Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Module Handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new EntityRouteProviderSubscriber instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   * @param \Drupal\crm_core_user_sync\CrmCoreUserSyncRelation $relation_manager
   *   User sync relation manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The ModuleHandler.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    CrmCoreUserSyncRelation $relation_manager,
    ConfigFactoryInterface $configFactory,
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->relationManager = $relation_manager;
    $this->configFactory = $configFactory;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Get the crm Individual type.
   *
   * @return string
   *   The Crm Core Individual Type.
   */
  protected function getIndividualType(): string {
    $type = $this->configFactory
      ->get('crm_core_commerce.settings')
      ->get('individual_type');

    if ($type === NULL) {
      throw new StorageException("The crm_core_commerce.settings:individual_type is not set.");
    }

    return $type;
  }

  /**
   * Get The Individual to map.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Order.
   *
   * @return \Drupal\crm_core_contact\IndividualInterface|null
   *   The Individual to update.
   */
  protected function getIndividual(OrderInterface $order): ?IndividualInterface {
    $type = $this->entityTypeManager
      ->getStorage('crm_core_individual_type')
      ->load($this->getIndividualType());
    $primary_fields = $type->getPrimaryFields();

    $crm_individual_storage = $this->entityTypeManager
      ->getStorage('crm_core_individual');

    // Find by user account.
    if ($order->getCustomerId() > 0) {
      $individual_id = $this->relationManager
        ->getIndividualIdFromUserId($order->getCustomerId());
      if ($individual_id) {
        return $crm_individual_storage->load($individual_id);
      }
    }

    // Find by entity reference.
    if ($order->get('crm_core_individual')->target_id) {
      $individual_id = $order->get('crm_core_individual')->target_id;
      return $crm_individual_storage->load($individual_id);
    }

    // Find by email without user account.
    if (!empty($primary_fields['email'])) {
      $results = $crm_individual_storage
        ->loadByProperties([$primary_fields['email'] => $order->getEmail()]);
      if (!empty($results)) {
        return reset($results);
      }
    }

    return NULL;
  }

  /**
   * Maps the Order profile data on an Individual.
   *
   * @inheritdoc
   */
  public function mapOrderToIndividual(OrderInterface $order): IndividualInterface {
    /** @var \Drupal\crm_core_contact\IndividualInterface $individual */
    $individual = $this->getIndividual($order);

    $data = $this->getData($order);

    if ($individual) {
      return $this->updateIndividual(
        $data,
        $individual
      );
    }
    return $this->createNewIndividual($data);

  }

  /**
   * Create a new Individual.
   *
   * @param array $data
   *   The formatted data to create an individual.
   */
  protected function createNewIndividual(array $data): IndividualInterface {
    return $this->entityTypeManager->getStorage('crm_core_individual')
      ->create($data);
  }

  /**
   * Update an existing Individual.
   *
   * @param array $data
   *   The formatted data to create an individual.
   * @param \Drupal\crm_core_contact\IndividualInterface $individual
   *   Individual.
   *
   * @return \Drupal\crm_core_contact\IndividualInterface
   *   The updated individual.
   *
   * @throws \Exception
   *   If the bundle of the entity would change.
   */
  protected function updateIndividual(
    array $data,
    IndividualInterface $individual
  ) : IndividualInterface {
    foreach ($data as $key => $value) {
      if ($key === 'type' && $individual->bundle() !== $value) {
        throw new \Exception('Trying to change bundle of individual on update.');
      }

      $individual->set($key, $value);
    }

    return $individual;
  }

  /**
   * Get the data from the order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to extract data from.
   *
   * @return array
   *   The individual data in array form.
   */
  protected function getData(OrderInterface $order) {
    $type = $this->getIndividualType();
    $data = [
      'type' => $type,
    ];

    $profile = $order->getBillingProfile();
    if ($profile instanceof ProfileInterface) {
      $primary_fields = $this->entityTypeManager
        ->getStorage('crm_core_individual_type')
        ->load($type)->getPrimaryFields();

      if (array_key_exists('address', $primary_fields)
        && !empty($primary_fields['address'])) {
        $data[$primary_fields['address']] = $profile->address->getValue()[0];
      }
      if (array_key_exists('email', $primary_fields)
        && !empty($primary_fields['email'])) {
        $data[$primary_fields['email']] = $order->getEmail();
      }

      $data['name'] =
        [
          'given' => $profile->address->given_name,
          'family' => $profile->address->family_name,
        ];
    }

    $this->moduleHandler->alter('crm_core_individual_data', $data, $order);

    return $data;
  }

}
